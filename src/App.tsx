import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Home } from './components/Home/Home';

const loader = document.getElementById('loader');

function App(): JSX.Element {
  useEffect(() => {
    loader?.classList.add('loader--hide');
  }, []);
  return (
    <BrowserRouter>
      <Home />
    </BrowserRouter>
  );
}

export default App;
