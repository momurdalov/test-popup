import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Modal from 'react-modal';
import styles from './Home.module.scss';
import { Checkbox, FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import close from '../../assets/icons/Vector.png';
import checked from '../../assets/icons/checked.png';
import { styled } from '@material-ui/styles';

const maxDeduction = 260000;
const minSalary = 12792;
const maxSalary = 2000000;

export const Home: React.FC = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [input, setInput] = useState(0);
  const [deductions, setDeductions] = useState<Number[]>();

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function onSubmit() {
    const deduction = input * 12 * 0.13;
    const fullDeduction = parseInt((maxDeduction / deduction).toString());
    const remainder = maxDeduction % deduction;

    const fullDeductionArray = Array(fullDeduction).fill(deduction);
    setDeductions([...fullDeductionArray, remainder]);
  }

  const BpIcon = styled('span')(({ theme }) => ({
    backgroundColor: '#FFFFFF',
    borderRadius: '6px',
    border: '1px solid #DFE3E6',
    width: '20px',
    height: '20px',
  }));

  const BpCheckedIcon = styled(BpIcon)({
    backgroundColor: '#FF5E56',
    borderRadius: '6px',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'flex',
      width: 18,
      height: 18,
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
      content: '""',
    },
  });

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <Button onClick={openModal} className={styles.btnOpen}>
          Налоговый вычет
        </Button>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          className={styles.modal}
          style={{ overlay: { backgroundColor: 'gray' } }}
          contentLabel="Example Modal"
        >
          <div onClick={closeModal} className={styles.closeBtn}>
            <img src={close} />
          </div>
          <div className={styles.modalCont}>
            <div className={styles.titleCont}>
              <p className={styles.title}>Налоговый вычет</p>
              <p className={styles.description}>
                Используйте налоговый вычет чтобы погасить ипотеку досрочно. Размер налогового
                вычета составляет не более 13% от своего официального годового дохода.
              </p>
            </div>
            <div className={styles.formCont}>
              <div className={styles.form}>
                <p className={styles.salaryTitle}>Ваша зарплата в месяц</p>
                <input
                  max={maxSalary}
                  min={minSalary}
                  pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$"
                  placeholder="Введите данные"
                  className={styles.salaryInput}
                  onChange={(e) => setInput(Number(e.target.value))}
                />
                <div>
                  <p onClick={() => onSubmit()} className={styles.calc}>
                    Рассчитать
                  </p>
                </div>

                {deductions && (
                  <p className={styles.checkboxTitle}>Итого можете внести в качестве досрочных: </p>
                )}
                {deductions?.map((e, i) => (
                  <div className={styles.checkboxForm}>
                    <FormControlLabel
                      {...e}
                      control={
                        <Checkbox
                          checkedIcon={<BpCheckedIcon />}
                          icon={<BpIcon />}
                          className={styles.checkbox}
                        />
                      }
                      label={
                        <span>
                          {e} рублей{' '}
                          {i != 1 ? (
                            <span className={styles.smallTextCheckbox}>в {i + 1}-ый год</span>
                          ) : (
                            <span className={styles.smallTextCheckbox}>во 2-ой год</span>
                          )}
                        </span>
                      }
                    />
                  </div>
                ))}
                <div className={styles.otherCont}>
                  <p className={styles.otherTitle}>Что уменьшаем?</p>
                  <div className={styles.formRadioBtnCont}>
                    <div className={styles.formRadioBtn}>
                      <input id="radio-1" type="radio" name="radio" value="1" checked />
                      <label htmlFor="radio-1">Платёж</label>
                    </div>
                    <div className={styles.formRadioBtn}>
                      <input id="radio-2" type="radio" name="radio" value="2" />
                      <label htmlFor="radio-2">Срок</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.addBtnCont}>
              <Button className={styles.addBtn}>
                <p>Добавить</p>
              </Button>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
};
